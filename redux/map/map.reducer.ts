import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Mods, Styles } from '@redux/map/map.const'
import * as Types from './map.types'

const pos = {
  moscow: {
    lng: 37.6165,
    lat: 55.7505,
    zoom: 10,
  },
  tyumen: {
    lng: 65.5558,
    lat: 57.1464,
    zoom: 13,
  },
}

const mapSlice = createSlice({
  name: 'map',
  initialState: {
    lng: pos.moscow.lng,
    lat: pos.moscow.lat,
    zoom: pos.moscow.zoom,
    style: Styles.MAIN,
    mod: Mods.DEFAULT,
    dragging: false,
  } as Types.State,
  reducers: {
    setCenter: (state, action: PayloadAction<Types.Center>) => {
      Object.assign(state, action.payload)
    },
    setZoom: (state, action: PayloadAction<number>) => {
      state.zoom = action.payload
    },
    setMod: (state, action: PayloadAction<string>) => {
      state.mod = action.payload
    },
    setViewport: (state, action: PayloadAction<Types.Viewport>) => {
      Object.assign(state, action.payload)
    },
    setStyle: (state, action: PayloadAction<string>) => {
      state.style = action.payload
    },
    setDragging: (state, action: PayloadAction<boolean>) => {
      state.dragging = action.payload
    },
  },
})

export const { reducer, actions } = mapSlice
