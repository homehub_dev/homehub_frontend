import block from 'bem-css-modules'
import { Formik } from 'formik'
import { AvailabilityRow } from '@features/pages/main/availability'
import { Rating } from '@features/pages/main/rating'
import { Checkbox } from '@ui/checkbox'
import style from './form_filter.module.sass'

const b = block(style)

export function FormFilter(): JSX.Element {
  return (
    <div className={b({ filter: true })}>
      <Formik initialValues={{ test: false }} validationSchema={{}} onSubmit={console.log}>
        {() => (
          <>
            <div>
              <Checkbox defaultChecked={false}>
                <Rating desc={'Качество жизни'} name={'name_1'} ranges={[1, 2, 3, 4, 5]} />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <Rating
                  desc={'Доступность социальной инфраструктуры'}
                  name={'name_2'}
                  ranges={[1, 2, 3, 4, 5]}
                />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <Rating desc={'Качество воздуха'} name={'name_3'} ranges={[1, 2, 3, 4, 5]} />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <Rating
                  desc={'Процент плотности застройки'}
                  name={'name_4'}
                  ranges={[1, 2, 3, 4, 5]}
                />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <Rating desc={'Процент застройки'} name={'name_5'} ranges={[1, 2, 3, 4, 5]} />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <Rating desc={'Рельеф местности'} name={'name_6'} ranges={[1, 2, 3, 4, 5]} />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <AvailabilityRow destination={'метро'} />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <AvailabilityRow destination={'школы'} />
              </Checkbox>
            </div>
            <div>
              <Checkbox defaultChecked={false}>
                <AvailabilityRow />
              </Checkbox>
            </div>
            <div>
              <button>Добавить условие</button>
              <button>Сбросить</button>
            </div>
          </>
        )}
      </Formik>
    </div>
  )
}
